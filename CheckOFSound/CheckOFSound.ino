/*struct Vision
{
  Sensor right;
  Sensor front;
}*/
struct Sensor
{
    //word power;
    word data;
    word output;
    
    word Read()
    {
      return pulseIn (data, HIGH);
    }
};

Sensor ultraSoung[2];

void setup() {
  ultraSoung[0].data = 11;
  ultraSoung[0].output = 10;
  ultraSoung[1].data = 9;
  ultraSoung[1].output = 8;

  pinMode (ultraSoung [0].data, INPUT);
  pinMode (ultraSoung [1].data, INPUT);

  pinMode (ultraSoung [0].output, OUTPUT);
  pinMode (ultraSoung [1].output, OUTPUT);

  Serial.begin (9600); 
}

void loop() {
  Serial.println (TakeDistanceBeforeObject (1));
  delay (100);
}

word TakeDistanceBeforeObject (byte i) //check
{
  //digitalWrite (ultraSoung [i].power, HIGH);
  
  digitalWrite (ultraSoung [i].output, HIGH);
  delayMicroseconds(10);
  digitalWrite (ultraSoung [i].output, LOW);
  
  int impulseTime = ultraSoung [i].Read();
  
  //digitalWrite (ultraSoung [i].power, LOW);
  
  return impulseTime / 58;
}
