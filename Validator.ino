//Validator
const int ThresholdOfDistance = 50;

bool SensorIsWathing (int distanceAtLeft, int distanceAtRight)
{
  if (distanceAtLeft < ThresholdOfDistance || distanceAtRight < ThresholdOfDistance)
    return true;
  return false;  
}

bool TwoSensorsAreWathing (int distanceAtLeft, int distanceAtRight)
{
  if (distanceAtLeft < ThresholdOfDistance && distanceAtRight < ThresholdOfDistance)
    return true;
  return false; 
}

/*bool TwoSensorsAreValid (int firstValue, int secondValue)
{
  if (firstValue < ThresholdOfValid && firstValue < ThresholdOfValid)
    return true;
  return false; 
}

/*bool AllSensorsAreValid (int firstValue, int secondValue, int thirdValue, int forthValue)
{
  if (firstValue < ThresholdOfValid && 
      secondValue < ThresholdOfValid && 
      thirdValue < ThresholdOfValid && 
      forthValue < ThresholdOfValid)
      
    return true;
    
  return false; 
}*/

bool SensorIsValid (int value)
{
  const int ThresholdOfValid = 350;
  
  if (value < ThresholdOfValid)
    return true;
  return false; 
}
