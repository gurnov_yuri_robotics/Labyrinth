struct Engine
{
  const int MAX_POWER_VALUE = 255;

  int forward;
  int back;
  int power;

  void ChangePower(int valueOfPower)
    {
      if (valueOfPower >= 0)
      {
        analogWrite (power, valueOfPower);
        digitalWrite (forward, HIGH);
        digitalWrite (back, LOW);
      }

      else
      {
        analogWrite (power, valueOfPower);
        digitalWrite (back, HIGH);
        digitalWrite (forward, LOW);
      }
      
    }

  void FullPower()
  {
    digitalWrite (power, HIGH);

    ChangePower(MAX_POWER_VALUE);
  }

  void NoPower()
  {
    digitalWrite (power, LOW);

    ChangePower(0);
  }
};

struct TheWholeEngine
{
  Engine left;
  Engine right;

  void NoPower()
  {
    left.NoPower();
    right.NoPower();
  }

  void FullPower()
  {
    left.FullPower();
    right.FullPower();
  }

  void rotateDegree (int numberOfDegree, char directionOfTurn) //2 - вправо
  {
    const int NORM_POWER_VALUE = 178;
    
    const float timeOfTurnOnOneDegree = 5.4f;
    float timeOfTurnOneStep = timeOfTurnOnOneDegree * numberOfDegree / 10.0f;

    if (directionOfTurn == 'L')
    {
      int i = 0;
      while (i < 10)
      {
        left.ChangePower (-NORM_POWER_VALUE);
        right.ChangePower (NORM_POWER_VALUE);

        delay (timeOfTurnOneStep);
        
        NoPower();

        delay (100);

        i++;
      }
    }
    
    else
    {
      int i = 0;
      while (i < 10)
      {
        left.ChangePower (NORM_POWER_VALUE);
        right.ChangePower (-NORM_POWER_VALUE);

        delay (timeOfTurnOneStep);
        
        NoPower();

        delay (100);

        i++;
      }
    }
  }

  void ChangeOfPower (word power, byte placementOfLine)
  {
    if (placementOfLine = 0)
      left.ChangePower (power);
    else
      right.ChangePower (power);
  }

  /*void ChangeOfAllPower (word power)
    {
    left.ChangeOfPower (power);
    right.ChangeOfPower (power);
    }*/
};

TheWholeEngine driver;

void setup() {

  driver.left.forward = 5;
  driver.left.back = 6;
  driver.left.power = 7;

  driver.right.power = 2;
  driver.right.forward = 3;
  driver.right.back = 4;

  pinMode (driver.left.power, OUTPUT);
  pinMode (driver.left.forward, OUTPUT);
  pinMode (driver.left.back, OUTPUT);

  pinMode (driver.right.power, OUTPUT);
  pinMode (driver.right.forward, OUTPUT);
  pinMode (driver.right.back, OUTPUT);

}

void loop()
{
  /*digitalWrite (driver.left.back, LOW);
  digitalWrite (driver.left.forward, HIGH);
  analogWrite (driver.left.power, 130 * 1.25);

  digitalWrite (driver.right.back, LOW);
  digitalWrite (driver.right.forward, HIGH);
  analogWrite (driver.right.power, 130);*/

  driver.rotateDegree (90, 'L');

  delay (1000);
}
