void DriveWithSmoothingOfLineRight ()
{
  const int differenceOfLeftEngine = 1.25;
  
  const int NORM_POWER_VALUE = 200;
  const int sizeOfTurn = 30;
  const int secureRightDistance = 7;
  
    //driver.ChangeOfAllPower(MAX_POWER_VALUE);

  int recentDistance = FindDistanceToLine (0);
    
    if (recentDistance < secureRightDistance)
    {
      driver.right.ChangePower (NORM_POWER_VALUE);
      driver.left.ChangePower ((NORM_POWER_VALUE - (secureRightDistance - recentDistance) * sizeOfTurn) * differenceOfLeftEngine); //проверено
    }
    
    else
    {
      driver.left.ChangePower (NORM_POWER_VALUE * differenceOfLeftEngine);
      driver.right.ChangePower (NORM_POWER_VALUE - (recentDistance - secureRightDistance) * sizeOfTurn); //проверено
    }
      
}
