

  int Sensor::Read()
  {
    return pulseIn (data, HIGH);
  }

  void Engine::ChangePower(int valueOfPower)
  {
    if (valueOfPower >= 0)
    {
      analogWrite (power, valueOfPower);
      digitalWrite (forward, HIGH);
      digitalWrite (back, LOW);
    }

    else
    {
      analogWrite (power, -valueOfPower);
      digitalWrite (back, HIGH);
      digitalWrite (forward, LOW);
    }
    
  }

  void Engine::FullPower()
  {
    digitalWrite (power, HIGH);
    
    ChangePower(NORM_POWER_VALUE);
  }

  void Engine::NoPower()
  {
    digitalWrite (power, LOW);

    ChangePower(0);
  }

  void DoubleEngine::ChangePower(int power)
  {
    left.ChangePower(power);
    right.ChangePower(power);
  }

  void DoubleEngine::FullPower()
  {
    left.FullPower();
    right.FullPower();
  }

  void DoubleEngine::NoPower()
  {
    left.NoPower();
    right.NoPower();
  }

  void DoubleEngine::rotateDegree (int numberOfDegree, char directionOfTurn) //2 - вправо
  {
    const float timeOfTurnOnOneDegree = 11.8f;
    float timeOfTurnOneStep = timeOfTurnOnOneDegree * numberOfDegree / 10.0f;

    if (directionOfTurn == 'L')
    {
      int i = 0;
      while (i < 10)
      {
        left.ChangePower (-NORM_POWER_VALUE);
        right.ChangePower (NORM_POWER_VALUE);

        delay (timeOfTurnOneStep);
        
        NoPower();

        delay (200);

        i++;
      }
    }
    
    else
    {
      int i = 0;
      while (i < 10)
      {
        left.ChangePower (NORM_POWER_VALUE);
        right.ChangePower (-NORM_POWER_VALUE);

        delay (timeOfTurnOneStep);
        
        NoPower();

        delay (100);

        i++;
      }
    }
  }
