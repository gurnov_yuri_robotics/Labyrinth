struct Sensor
{
    int data;
    int output;
    
    int Read();
};

struct Engine
{
    const int NORM_POWER_VALUE = 200;
  
    int forward;
    int back;
    int power;

    void ChangePower(int valueOfPower);

    void FullPower();

    void NoPower();
};

struct DoubleEngine
{
    const int NORM_POWER_VALUE = 200;
  
    Engine right;
    Engine left;

    void ChangePower(int power);

    void FullPower();

    void NoPower();

    void rotateDegree (int numberOfDegree, char directionOfTurn);
};

DoubleEngine driver;

Sensor ultraSound[1];

void setup() {

  driver.left.forward = 5;
  driver.left.back = 6;
  driver.left.power = 7;

  driver.right.power = 2;
  driver.right.forward = 3;
  driver.right.back = 4;
  
  pinMode (driver.left.power, OUTPUT);
  pinMode (driver.left.forward, OUTPUT);
  pinMode (driver.left.back, OUTPUT);

  pinMode (driver.right.power, OUTPUT);
  pinMode (driver.right.forward, OUTPUT);
  pinMode (driver.right.back, OUTPUT);
  
  ultraSound[0].data = 11;
  ultraSound[0].output = 10;
  ultraSound[1].data = 9;
  ultraSound[1].output = 8;

  pinMode (ultraSound [0].data, INPUT);
  pinMode (ultraSound [1].data, INPUT);

  pinMode (ultraSound [0].output, OUTPUT);
  pinMode (ultraSound [1].output, OUTPUT);

  Serial.begin (9600);  
}

const word secureFrontDistance = 9;
const word secureRightDistance = 7;
const int MAX_POWER_VALUE = 200;

void loop() 
{
  Serial.println (FindDistanceToLine (0));
  Serial.println (FindDistanceToLine (1));
  
  if (FindDistanceToLine (0) > secureRightDistance * 2)
  {
    Serial.println ("Turn at right");
    driver.rotateDegree (90, 'R');
  }

  else

  {
    if (FindDistanceToLine (1) < secureFrontDistance)
    {
      driver.rotateDegree (90, 'L');
      Serial.println ("Turn at left");
    }
      
    else

    {
      Serial.println ("DriveWithSmoothingOfLineRight");
      driver.FullPower();
      DriveWithSmoothingOfLineRight();
    }

  }

  delay (50);

}

