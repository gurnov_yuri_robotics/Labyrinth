void Braking ()
{
  const int NORM_POWER_VALUE = 150;
  
  driver.left.ChangePower (-NORM_POWER_VALUE);
  driver.right.ChangePower (-NORM_POWER_VALUE);
  delay (50);

  driver.NoPower();
}

